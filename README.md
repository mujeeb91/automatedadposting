## Description

This Python script is for creating and deleting OLX ad(s). It,

1. Logs in a user
2. Deactivates existing ad(s)
3. Deletes deactivated ad(s)
4. Posts ad(s) on OLX

---

## How to use


---

## Limitations

The limitations of this script are as follows:

1. Developed for Linux (with Linux file structure)
2. Selects "best" category recommended by OLX
3. Uses **Firefox** as the browser.
4. OLX does not allow duplicate ads. [What makes an ad duplicate?](https://help.olx.com.pk/hc/en-us/articles/360000057461-Ad-Rejection-Reason-Duplicates)
