import configparser
from datetime import datetime
from difflib import SequenceMatcher
import itertools
import json
import operator
import os
from os import listdir
import re
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ExpectedConditions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import time

config = configparser.RawConfigParser()
config.read('config.cfg')
MIN_TITLE_LIMIT = 15
MAX_TITLE_LIMIT = 70
DELAY = config.getint("Configuration", "delay")
ALLOWED_SIMILARITY = config.getfloat("Configuration", "allowed_similarity")


def main():
    driver = init_driver()
    login(driver)
    ads_to_upload = get_ads()
    retry_attempts = config.getint("Configuration", "retry_attempts")
    for ad in ads_to_upload:
        for i in range(retry_attempts):
            try:
                process_ad(driver, ad)
                print("Successfully posted ad")
                break
            except Exception as e:
                print("Failed to post ad\nReason: ", e)

    driver.quit()


def process_ad(driver, ad):
    previous_title, previous_description = remove_ad(driver, ad["products"])
    post_ad(driver, ad, previous_title, previous_description)


def init_driver():
    capabilities = DesiredCapabilities().FIREFOX
    capabilities["pageLoadStrategy"] = "eager"
    driver = webdriver.Firefox(executable_path=os.getcwd() + "/geckodriver", desired_capabilities=capabilities)
    return driver


def remove_ad(driver, products):
    title = find_existing_title(driver, products)
    description = find_existing_description(driver, title)
    if title and not config.getboolean("Configuration", "debug") and False:
        deactivate_ad(driver, title)
        delete_ad(driver, title)
    else:
        print("Ad does not exist")

    return title, description


def login(driver):
    login_url = "https://www.olx.com.pk/account/"
    driver.get(login_url)

    email_field = driver.find_element_by_id("userEmail")
    email_field.send_keys(config.get("Credentials", "email"))
    password_field = driver.find_element_by_id("userPass")
    password_field.send_keys(config.get("Credentials", "password"))
    driver.find_element_by_id("se_userLogin").click()


def get_ads():
    ads = []
    ads_path = os.getcwd()+"/ads"
    for directory_name in os.listdir(ads_path):
        directory_path = ads_path + "/" + directory_name
        ad_file = [directory_path+"/"+pos_json for pos_json in os.listdir(directory_path) if pos_json.endswith('.json')][0]
        with open(ad_file) as f:
            ad = json.load(f)
            ad['images'] = sorted([directory_path + "/images/" + f for f in listdir(directory_path + "/images/")])
            ads.append(ad)

    return ads


def find_existing_title(driver, products):
    url = "https://www.olx.com.pk/myaccount/"
    driver.get(url)

    for ad_div in driver.find_element_by_id("adsTable").find_elements_by_xpath('.//h3'):
        ad_title = ad_div.get_attribute("title")
        for product in products:
            product_name_exists_in_title = False
            for name in product["names"]:
                if name in ad_title:
                    product_name_exists_in_title = True
                    break
            if not product_name_exists_in_title:
                break  # This is not the ad we're looking for
            if product_name_exists_in_title and product == products[len(products)-1]:
                return ad_title


def find_existing_description(driver, title):
    driver.find_element_by_xpath(
        '//h3[@title="' + title + '"]/parent::*/parent::*/parent::*/parent::*/parent::*').find_element_by_xpath(
        './/a[@title="Edit"]').click()
    description_field = WebDriverWait(driver, DELAY).until(
        ExpectedConditions.presence_of_element_located((By.ID, "add-description"))
    )
    description = description_field.get_attribute('value')
    return description


def deactivate_ad(driver, title):
    url = "https://www.olx.com.pk/myaccount/"
    driver.get(url)
    driver.find_element_by_xpath(
        '//h3[@title="' + title + '"]/parent::*/parent::*/parent::*/parent::*/parent::*').find_element_by_xpath(
        './/a[@title="Deactivate Ad"]').click()
    driver.find_element_by_id("reason-4").click()
    driver.find_element_by_id("deleteButton").click()


def delete_ad(driver, title):
    url = "https://www.olx.com.pk/myaccount/archive/"
    driver.get(url)
    driver.find_element_by_xpath(
        '//h3[@title="' + title + '"]/parent::*/parent::*/parent::*/parent::*/parent::*').find_element_by_xpath(
        './/a[@title="Delete Ad"]').click()


def add_images(driver, images):
    driver.find_element_by_css_selector('input[type="file"]').clear()
    driver.find_element_by_css_selector('input[type="file"]').send_keys("\n".join(images))
    wait_till_upload_completes(driver, len(images))


def wait_till_upload_completes(driver, image_count):
    upload_in_progress = True
    t_end = time.time() + (config.getint("Configuration", "max_upload_time_per_image") * image_count)
    while time.time() < t_end and upload_in_progress:
        upload_complete = True
        for i in range(1, image_count+1):
            try:
                driver.find_element_by_id("img_{}".format(i)).get_attribute("src")
                is_last_image = i == image_count
                if is_last_image and upload_complete:
                    upload_in_progress = False
                    break
            except Exception as e:
                upload_complete = False
                break


def post_ad(driver, ad, previous_title, previous_description):
    post_url = "https://www.olx.com.pk/posting/"
    driver.get(post_url)

    # Sets Ad title
    title_field = driver.find_element_by_id("add-title")
    titles = generate_titles(ad["products"])
    title = get_next_item(previous_title, titles)
    title_field.send_keys(title)

    # Select best matching category
    driver.find_element_by_id("targetrenderSelect1-0").click()
    time.sleep(DELAY)  # driver.implicitly_wait() does not wait until best category suggestions are fetched
    WebDriverWait(driver, DELAY).until(
        ExpectedConditions.visibility_of_element_located((By.ID, "fancybox-overlay"))
    )
    best_matching_category_field = WebDriverWait(driver, DELAY).until(
        ExpectedConditions.presence_of_element_located((By.XPATH, "//div[@id='icons-placeholder']/ul[1]/li[1]/div[1]/a[1]"))
    )
    best_matching_category_field.click()

    # Waits for category modal to close
    WebDriverWait(driver, DELAY).until(
        ExpectedConditions.invisibility_of_element_located((By.ID, "fancybox-overlay"))
    )

    # Wait for price field to be clickable
    WebDriverWait(driver, DELAY).until(
        ExpectedConditions.element_to_be_clickable((By.NAME, "data[param_price][1]"))
    )

    # Sets product price
    price_field = driver.find_element_by_name("data[param_price][1]")
    price_field.send_keys(ad["price"])

    # Sets product condition
    driver.find_element_by_xpath("//dl[@id='targetparam26']/dt/a").click()
    if ad['is_new']:
        driver.find_element_by_xpath("//dl[@id='targetparam26']/dd/ul/li[2]/a").click()
    else:
        driver.find_element_by_xpath("//dl[@id='targetparam26']/dd/ul/li[3]/a").click()

    # Sets product description
    descriptions = generate_descriptions(ad["description"])
    least_similar_descriptions = get_least_similar_descriptions(descriptions)
    unique_descriptions = add_unique_words_to_description(least_similar_descriptions, titles, ad["insert_index"])
    benchmark(unique_descriptions)
    description = get_next_item(previous_description, unique_descriptions)
    driver.find_element_by_id("add-description").send_keys(description)

    # Uploads product images
    if not config.getboolean("Configuration", "debug"):
        add_images(driver, ad["images"])

    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # Set seller district
    driver.find_element_by_id("district-input").click()
    driver.find_element_by_xpath("//li[text()='{}']".format(config.get("Configuration", "district"))).click()

    # Submit Ad
    if not config.getboolean("Configuration", "debug"):
        driver.find_element_by_id("save").click()

    # Wait for form to submit
    time.sleep(DELAY)


def generate_titles(products):
    products_titles = []
    for product in products:
        product_wise_titles = []
        for element in itertools.product(*[product["names"], product["features"]]):
            title = " ".join(element)
            if len(title) < MIN_TITLE_LIMIT:
                title += " for Sale"
            elif len(title) > MAX_TITLE_LIMIT:
                title = title[:MAX_TITLE_LIMIT]
            product_wise_titles.append(title)
        products_titles.append(product_wise_titles)

    final_titles = []
    for element in itertools.product(*[item for item in products_titles]):
        final_titles.append(" and ".join(element))
    return final_titles


def generate_descriptions(description):
    options = get_all_options(description)
    options = [option.split("|") for option in options]

    s = re.sub('{.*?}', '{}', description)

    descriptions = []
    for combination in itertools.product(*options):
        descriptions.append(s.format(*combination))

    return descriptions


def get_all_options(description):
    regex = r"\{(.*?)\}"
    options = []
    test_str = description

    matches = re.finditer(regex, test_str, re.MULTILINE | re.DOTALL)

    for matchNum, match in enumerate(matches):
        for groupNum in range(0, len(match.groups())):
            options.append(match.group(1))

    return options


def get_next_item(previous_item, items):
    try:
        previous_index = items.index(previous_item)
        next_index = previous_index + 1 if previous_index != len(items) - 1 else 0
    except ValueError:
        next_index = 0
    next_item = items[next_index]
    return next_item


def benchmark(descriptions):
    for i in range(len(descriptions)):
        desc_to_evaluate = descriptions[i]
        j = i+1
        while j < len(descriptions):
            ratio = SequenceMatcher(None, desc_to_evaluate, descriptions[j]).ratio()
            print("Ratio of {} and {}: {}".format(i, j, ratio))
            j += 1


def unique(items, compare=operator.eq):
    # compare is a function that returns True if its two arguments are deemed similar to
    # each other and False otherwise.

    unique_items = []
    for item in items:
        if not any(compare(item, uniq) for uniq in unique_items):
            # any will stop as soon as compare(item, uniq) returns True
            # you could also use `if all(not compare(item, uniq) ...` if you prefer
            unique_items.append(item)

    return unique_items


def description_cmp(candidate_desc, unique_desc):
    similarity_ratio = SequenceMatcher(None, unique_desc, candidate_desc).ratio()
    return similarity_ratio > ALLOWED_SIMILARITY


def get_least_similar_descriptions(descriptions):
    return unique(descriptions, compare=description_cmp)


def add_unique_words_to_description(descriptions, titles, insert_index):
    unique_descriptions = []
    greetings = open("greetings.txt", "r").read().splitlines()
    closings = open("closings.txt", "r").read().splitlines()
    for i, description in enumerate(descriptions): # Add salutation and closing
        description = inject_at_nth(description, "\n", "\n"+titles[i]+"\n", insert_index)
        description = str(greetings[i]) + "\n\n" + description
        description = description + "\n\n" + str(closings[i])
        unique_descriptions.append(description)
    return unique_descriptions


def inject_at_nth(s, sub, repl, nth):
    find = s.find(sub)
    # if find is not p1 we have found at least one match for the substring
    i = find != -1
    # loop util we find the nth or we find no match
    while find != -1 and i != nth:
        # find + 1 means we start at the last match start index + 1
        find = s.find(sub, find + 1)
        i += 1
    # if i  is equal to nth we found nth matches so we add the desired string after it
    if i == nth:
        return s[:find+1]+repl+s[find + len(sub):]
    return s


print("Script started on ", datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
main()
print("Script ended on ", datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
